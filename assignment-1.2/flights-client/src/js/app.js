import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import {createStore} from 'redux';
import {Provider} from 'react-redux';

import indexReducers from './reducers/index.reducers';

import AdminPage from './components/AdminPage/AdminPage.container';
import Error404 from './components/Error404/Error404.component';
import LandingPage from './components/LandingPage/LandingPage.component';
import Layout from './components/Layout/Layout.component';
import Login from './components/Login/Login.container';
import RedirectPage from './components/RedirectPage/RedirectPage.container';
import UserPage from './components/UserPage/UserPage.container';

import '../sass/style.sass';

let Store = createStore(indexReducers);

export default class App extends Component {
  render() {
    return (
      <Provider store={Store}>
        <Layout>
          <BrowserRouter>
            <Switch>
              <Route exact path="/" component={Login}/>
              <Route path="/user-flights" component={UserPage}/>
              <Route path="/admin-flights" component={AdminPage}/>
              <Route path="/redirect" component={RedirectPage}/>
              <Route component={Error404}/>
            </Switch>
          </BrowserRouter>
        </Layout>
      </Provider>
    );
  }
}
ReactDOM.render(
  <App/>,
  document.getElementById('root')
);
