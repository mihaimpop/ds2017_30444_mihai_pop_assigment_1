import {combineReducers} from 'redux';

import {loginReducer} from '../components/Login/Login.reducer';

const indexReducers = combineReducers({
  loginInfo: loginReducer,
});

export default indexReducers;
