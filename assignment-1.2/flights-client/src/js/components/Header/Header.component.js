import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {MdAirplanemodeActive} from 'react-icons/lib/md';

export default class Header extends Component {

  constructor(props) {
    super(props);
  }

  goHome = () => {
    location.href = '/';
  };

  render(){
    return(
      <div className="header-title" onClick={this.goHome}>
        <span>flights. <MdAirplanemodeActive/></span>
      </div>
    );
  }
}
