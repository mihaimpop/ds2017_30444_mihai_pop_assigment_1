import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Button, InputGroup, InputGroupAddon, Input} from 'reactstrap';

export default class Login extends Component {
  static propTypes = {
    getUserInfo: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      userName: null
    };
  }

  redirect = () => {
    this.props.history.push('/redirect');
  };

  setUserName = (e) => {
    this.setState({
      userName: e.target.value
    });
  };

  submit = () => {
    this.props.getUserInfo(this.state.userName)
      .then(userInfo => {
        this.redirect(userInfo);
      });
  };

  render(){
    return(
      <div className="login-form">
        <InputGroup>
          <InputGroupAddon>@</InputGroupAddon>
          <Input placeholder="username" onChange={this.setUserName}/>
        </InputGroup>
        <Button color="primary" onClick={this.submit}>Go to flights</Button>
      </div>
    );
  }
}
