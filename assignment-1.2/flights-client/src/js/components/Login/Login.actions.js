import {getUser} from '../../../services/resources/user';

export const GET_USER_INFORMATION = 'GET_USER_INFORMATION';
export const SET_REQUEST_AUTH_CREDENTIALS = 'SET_REQUEST_AUTH_CREDENTIALS';

export const getUserInfo = (userName, password) => (dispatch) => {
  return getUser(userName, password).then(userInfo => {
    dispatch({
      data: userInfo,
      type: GET_USER_INFORMATION
    })
  })
};

export const setRequestAuthCredentials = (username, password) => (dispatch) => {
 dispatch()
};
