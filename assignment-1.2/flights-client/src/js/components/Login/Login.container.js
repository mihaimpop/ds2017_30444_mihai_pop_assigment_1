import {connect} from 'react-redux';

import Login from './Login.component';
import {getUserInfo} from './Login.actions';

const mapStateToProps = (state) => ({
  loginInfo: state.loginInfo.get('userInfo')
});

const mapDispatchToProps = (dispatch) => ({
  getUserInfo: (userName, password) => getUserInfo(userName, password)(dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
