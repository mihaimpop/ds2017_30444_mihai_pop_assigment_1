import {Map} from 'immutable';

import {GET_USER_INFORMATION} from './Login.actions.js';

const init = Map({
  userInfo: {}
});

export const loginReducer = (state = init, action) => {
  switch (action.type) {
    case GET_USER_INFORMATION: {
      return state.set('userInfo', action.data);
    }
    default:
      return state;
  }
};