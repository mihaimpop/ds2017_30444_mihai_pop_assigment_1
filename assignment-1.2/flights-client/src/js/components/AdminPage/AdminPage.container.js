import {connect} from 'react-redux';

import AdminPage from './AdminPage.component';
import {getUser} from '../../../services/resources/user';

const mapStateToProps = (state) => ({
  loginInfo: state.loginInfo.get('userInfo')
});

const mapDispatchToProps = (dispatch) => ({
  getUserInfo: (userName, password) => getUser(userName)
});

export default connect(mapStateToProps, mapDispatchToProps)(AdminPage);
