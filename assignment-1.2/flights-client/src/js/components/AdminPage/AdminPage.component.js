import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Button, InputGroup, InputGroupAddon, Input} from 'reactstrap';
import {MdSearch} from 'react-icons/lib/md';

import AddFlight from '../AddFlightModal/AddFlight';
import CardsLayout from '../CardsLayout/CardsLayout.component';
import AdminModal from '../AdminModal/AdminModal.component';
import ROLES from '../../../services/roles';
import {getFlights} from '../../../services/resources/flight';

export default class AdminPage extends Component {
  static propTypes = {
    loginInfo: PropTypes.object,
    getUserInfo: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.state = {
      flights: [],
      modal: false,
      searchString: null,
      unfilteredFlights: []
    };
  }

  componentWillMount() {
    this.props.getUserInfo(localStorage.getItem('username'))
      .then((userInfo) => {
        if (userInfo && userInfo.data.role !== ROLES.TYPE.ADMIN) {
          this.props.history.push('/unauthorized');
        }
      });
  }

  componentDidMount() {
    this.getFlightsInfo();
  }

  getFlightsInfo = () => {

    getFlights().then(({data}) => {
      this.setState({flights: data, unfilteredFlights: data});
    });
  };

  openInfoModal = (data) => {
    this.modal.toggle(data);
  };

  filterList = (e) => {
    const searchString = e.target.value;

    let filteredFlights = this.state.flights
      .filter(({airplaneType, arrivalCity, departureCity, flightNumber}) => {
        return airplaneType.toLowerCase().includes(searchString) ||
          departureCity.toLowerCase().includes(searchString) ||
          arrivalCity.toLowerCase().includes(searchString) ||
          departureCity.toLowerCase().includes(searchString) ||
          flightNumber.toString().includes(searchString);
      });
    if(!filteredFlights.length) {
      filteredFlights = this.state.unfilteredFlights;
    }
    this.setState({
      flights: searchString === '' ? this.state.unfilteredFlights : filteredFlights
    });
  };

  openAddFlight = () => {
    this.addFlight.toggle();
  };

  render() {
    return (
      <div className="admin-page">
        <div className="page-title">
          <span>@admin</span>
          <Button className="add-flight-button"
                  onClick={this.openAddFlight}
                  color="primary">Add flight</Button>
          <div className="search-container">
            <InputGroup>
              <Input onChange={this.filterList} placeholder="Search..."/>
              <InputGroupAddon><MdSearch/></InputGroupAddon>
            </InputGroup>
          </div>
        </div>
        <AddFlight ref={(elem) => {this.addFlight = elem;}}
                   updateList={this.getFlightsInfo}/>
        <AdminModal ref={(elem) => {this.modal = elem;}}
                    openModal={this.openInfoModal}
                    updateList={this.getFlightsInfo}/>
        <CardsLayout data={this.state.flights} openModal={this.openInfoModal}/>
      </div>
    );
  }
}
