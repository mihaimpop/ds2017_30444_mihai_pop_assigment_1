import React, {Component} from 'react';

import Header from '../Header/Header.component';

export default class Layout extends Component {
  render(){
    return(
      <div className="app-container">
        <Header/>
        {this.props.children}
      </div>
    );
  }
}
