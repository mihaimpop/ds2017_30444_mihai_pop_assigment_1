import React, {Component} from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import {InputGroup, InputGroupAddon, Input} from 'reactstrap';
import {MdAccessTime} from 'react-icons/lib/md';

import httpResource from '../../../services/httpResource';


export default class LocalTime extends Component {
  static propTypes = {
    departureCity: PropTypes.string,
    departureTime: PropTypes.number,
    arrivalCity: PropTypes.string,
    arrivalTime: PropTypes.number
  };

  static GEO_API = 'https://maps.googleapis.com/maps/api/geocode/json?address=';
  static TIME_API = 'https://maps.googleapis.com/maps/api/timezone/json?location=';
  static LOCATION_API_KEY = 'AIzaSyCcfnY3VPKAIrcEJEzL0vjaaJocb9vp6Ac';
  static TIME_API_KEY = 'AIzaSyABv7g8YEHytvYzlt5_H6UwunkMITPjWGk';

  constructor(props) {
    super(props);
    this.state = {
      localTimes: null
    };
  }

  componentDidMount() {
    this.getLocalTimes();
  }

  getLocalTimes = () => {
    const cities = [this.props.arrivalCity, this.props.departureCity];
    let localTimes = {};
    const departureDate = new Date(this.props.departureTime);
    const arrivalDate = new Date(this.props.arrivalTime);
    let departureTime = departureDate.getTime() / 1000 + departureDate.getTimezoneOffset() * 60;
    let arrivalTime = arrivalDate.getTime() / 1000 + arrivalDate.getTimezoneOffset() * 60;
    let timeURL, offsets;
    let promise = Promise.resolve(cities.forEach((city, index) => {
      let geoUrl = `${LocalTime.GEO_API}${encodeURIComponent(city)}&key=${LocalTime.LOCATION_API_KEY}`;
      let location = {};
      return httpResource.get(geoUrl).then((response) => {
        location = response.data.results[0].geometry.location;
          if (index === 0) {
            timeURL = `${LocalTime.TIME_API}${location.lat},${location.lng}&timestamp=${departureTime}&key=${LocalTime.TIME_API_KEY}`;
            return httpResource.get(timeURL).then((response) => {
              offsets = response.data.dstOffset * 1000 + response.data.rawOffset * 1000;
              localTimes[index] = new Date(departureTime * 1000 + offsets);
            });
          } else {
            timeURL = `${LocalTime.TIME_API}${location.lat},${location.lng}&timestamp=${arrivalTime}&key=${LocalTime.TIME_API_KEY}`;
            return httpResource.get(timeURL).then((response) => {
              offsets = response.data.dstOffset * 1000 + response.data.rawOffset * 1000;
              localTimes[index] = new Date(arrivalTime * 1000 + offsets);
            });
          }
        });
    }));
    return promise.then(() =>
      this.setState({
        localTimes: localTimes
      }));
  };

  render() {
    return (
      <div className="local-time-container">
        <InputGroup>
          <InputGroupAddon>
            <MdAccessTime/>{'  '}
            {this.props.departureCity}
          </InputGroupAddon>
          <div>{this.state.localTimes && moment(this.state.localTimes[0]).format('MMMM Do YYYY, h:mm:ss a')}</div>
        </InputGroup>
        <InputGroup>
          <InputGroupAddon>
            <MdAccessTime/>{'  '}
            {this.props.arrivalCity}
          </InputGroupAddon>
          <div>{this.state.localTimes && moment(this.state.localTimes[1]).format('MMMM Do YYYY, h:mm:ss a')}</div>
        </InputGroup>
      </div>
    );
  }
}
