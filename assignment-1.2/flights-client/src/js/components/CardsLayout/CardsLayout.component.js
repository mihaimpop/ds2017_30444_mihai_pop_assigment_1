import React, {Component} from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import {
  Card, CardBody, CardText, CardTitle, CardSubtitle,
  Container, Row, Col, Button
} from 'reactstrap';
import {MdAccessTime, MdAirplanemodeActive, MdPlace} from 'react-icons/lib/md';
import {FaTicket} from 'react-icons/lib/fa/';

export default class CardsLayout extends Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    data: PropTypes.array,
    openModal: PropTypes.func
  };

  renderCards = (data) => {
    return data.map(({airplaneType, departureCity, departureTime, arrivalCity, arrivalTime, flightNumber}, index) =>
      <Card key={`card-${index}`} onClick={this.props.openModal.bind(null, data[index])}>
        <CardTitle>Flight #{flightNumber}</CardTitle>
        <CardSubtitle>
          <CardBody>
            <Container>
              <Row className="icons-container">
                <Col><FaTicket/></Col>
                <Col><MdAirplanemodeActive/></Col>
                <Col><MdPlace/></Col>
                <Col><MdAccessTime/></Col>
                <Col><MdPlace/></Col>
                <Col><MdAccessTime/></Col>
              </Row>
              <Row>
                <Col>#{flightNumber}</Col>
                <Col>{airplaneType}</Col>
                <Col>{departureCity}</Col>
                <Col>{moment(departureTime).format('MMMM Do YYYY, h:mm:ss a')}</Col>
                <Col>{arrivalCity}</Col>
                <Col>{moment(arrivalTime).format('MMMM Do YYYY, h:mm:ss a')}</Col>
              </Row>
            </Container>
          </CardBody>
        </CardSubtitle>
      </Card>
    );
  };

  render() {
    let cards = this.props.data !== [] && this.renderCards(this.props.data);
    return (
      <div className="cards-container">
        {
          this.props.data !== []
            ? this.renderCards(this.props.data)
            : <div>No data</div>
        }
      </div>
    );
  }
}
