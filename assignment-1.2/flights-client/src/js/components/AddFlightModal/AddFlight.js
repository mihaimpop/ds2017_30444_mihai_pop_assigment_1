import React, {Component} from 'react';
import PropTypes from 'prop-types';
import DatePicker from 'react-date-picker';
import moment from 'moment';
import {
  Button, ListGroup, ListGroupItem, Modal, ModalHeader, ModalBody, ModalFooter,
  InputGroup, InputGroupAddon, Input
} from 'reactstrap';
import {MdAccessTime, MdAirplanemodeActive, MdPlace} from 'react-icons/lib/md';
import {FaTicket} from 'react-icons/lib/fa/';

import {addFlight} from "../../../services/resources/flight";

export default class AddFlight extends Component {
  static propTypes = {
    openModal: PropTypes.func,
    updateList: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      flightNumber: '',
      airplaneType: '',
      departureCity: '',
      departureTime: null,
      arrivalCity: '',
      arrivalTime: null
    };
  }

  toggle = () => {
    this.setState({
      modal: !this.state.modal
    });
  };

  close = () => {
    this.setState({
      modal: !this.state.modal
    });
  };

  setFlightNumber = (e) => this.setState({flightNumber: e.target.value});

  setAirplaneType = (e) => this.setState({airplaneType: e.target.value});

  setDepartureCity = (e) => this.setState({departureCity: e.target.value});

  setDepartureTime = (departureDate) => this.setState({departureTime: departureDate.getTime()});

  setArrivalCity = (e) => this.setState({arrivalCity: e.target.value});

  setArrivalTime = (arrivalDate) => this.setState({arrivalTime: arrivalDate.getTime()});

  add = () => {
    const flight = {
      flightNumber: this.state.flightNumber,
      airplaneType: this.state.airplaneType,
      departureCity: this.state.departureCity,
      departureTime: this.state.departureTime,
      arrivalCity: this.state.arrivalCity,
      arrivalTime: this.state.arrivalTime
    };

    addFlight(flight).then(() => {
      this.props.updateList();
      this.close();
    });
  };

  render() {
    return (
      <div className="admin-modal-container">
        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
          <ModalHeader toggle={this.toggle}>Add flight #{this.state.flightNumber}</ModalHeader>
          <ModalBody>
            <InputGroup>
              <InputGroupAddon><FaTicket/></InputGroupAddon>
              <Input onChange={this.setFlightNumber}
                     placeholder="Flight number"/>
            </InputGroup>

            <InputGroup>
              <InputGroupAddon><MdAirplanemodeActive/></InputGroupAddon>
              <Input onChange={this.setAirplaneType}
                     placeholder="Airplane Type"/>
            </InputGroup>

            <InputGroup>
              <InputGroupAddon><MdPlace/></InputGroupAddon>
              <Input onChange={this.setDepartureCity}
                     placeholder="Departure city"/>
            </InputGroup>

            <InputGroup>
              <InputGroupAddon><MdAccessTime/></InputGroupAddon>
              <ListGroup>
                <ListGroupItem>
                  <DatePicker
                    id="departure-date"
                    minDate={new Date(0)}
                    maxDate={new Date(2019, 12, 12)}
                    onChange={this.setDepartureTime}
                    returnValue="start"
                    value={this.state.departureTime ? new Date(this.state.departureTime) : new Date()}
                  />
                </ListGroupItem>
              </ListGroup>

            </InputGroup>

            <InputGroup>
              <InputGroupAddon><MdPlace/></InputGroupAddon>
              <Input onChange={this.setArrivalCity}
                     placeholder="Arrival city"/>
            </InputGroup>

            <InputGroup>
              <InputGroupAddon><MdAccessTime/></InputGroupAddon>
              <ListGroup>
                <ListGroupItem>
                  <DatePicker
                    id="arrival-date"
                    minDate={new Date(1970, 1, 1)}
                    maxDate={new Date(2019, 12, 12)}
                    onChange={this.setArrivalTime}
                    returnValue="end"
                    value={this.state.arrivalTime ? new Date(this.state.arrivalTime) : new Date()}
                  />
                </ListGroupItem>
              </ListGroup>
            </InputGroup>

          </ModalBody>
          <ModalFooter>
            <Button color="success" onClick={this.add}>Update</Button>
            <Button color="secondary" onClick={this.toggle}>Close</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}