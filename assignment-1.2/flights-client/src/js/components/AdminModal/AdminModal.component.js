import React, {Component} from 'react';
import PropTypes from 'prop-types';
import DatePicker from 'react-date-picker';
import moment from 'moment';
import {Button, ListGroup, ListGroupItem, Modal, ModalHeader, ModalBody, ModalFooter,
  InputGroup, InputGroupAddon, Input} from 'reactstrap';
import {MdAccessTime, MdAirplanemodeActive, MdPlace} from 'react-icons/lib/md';
import {FaTicket} from 'react-icons/lib/fa/';

import {deleteFlight, updateFlight} from "../../../services/resources/flight";

export default class AdminModal extends Component {
  static propTypes = {
    openModal: PropTypes.func,
    updateList: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.state = {
      data: null || props.data,
      modal: false,
      flightNumber: '',
      airplaneType: '',
      departureCity: '',
      departureTime: '',
      arrivalCity: '',
      arrivalTime: ''
    };
  }

  toggle = (data) => {
    this.setState({
      data: data,
      modal: !this.state.modal
    });
  };

  close = () => {
    this.setState({
      modal: !this.state.modal
    });
  };

  setFlightNumber = (e) => this.setState({data: {
    ...this.state.data,
    flightNumber: e.target.value
  }});

  setAirplaneType = (e) => this.setState({data: {
    ...this.state.data,
    airplaneType: e.target.value
  }});

  setDepartureCity = (e) => this.setState({data: {
    ...this.state.data,
    departureCity: e.target.value
  }});

  setDepartureTime = (departureDate) => this.setState({data:{
    ...this.state.data,
    departureTime: departureDate.getTime()
  }});

  setArrivalCity = (e) => this.setState({data: {
    ...this.state.data,
    arrivalCity: e.target.value
  }});

  setArrivalTime = (arrivalDate) => this.setState({data:{
    ...this.state.data,
    arrivalTime: arrivalDate.getTime()
  }});

  delete = () => {
    deleteFlight(this.state.data.flightNumber).then(() => {
      this.props.updateList();
      this.close();
    });
  };

  update = () => {
    updateFlight(this.state.data).then(() => {
      this.props.updateList();
      this.close();
    });
  };

  render() {
    return (
      <div className="admin-modal-container">
          {
            this.state.data ?
              <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                <ModalHeader toggle={this.toggle}>Flight #{this.state.data.flightNumber} Information</ModalHeader>
                <ModalBody>
                  <InputGroup>
                    <InputGroupAddon><FaTicket/></InputGroupAddon>
                    <Input disabled
                           onChange={this.setFlightNumber}
                           placeholder={`#${this.state.data.flightNumber}`}/>
                  </InputGroup>

                  <InputGroup>
                    <InputGroupAddon><MdAirplanemodeActive/></InputGroupAddon>
                    <Input onChange={this.setAirplaneType}
                           placeholder={this.state.data.airplaneType}/>
                  </InputGroup>

                  <InputGroup>
                    <InputGroupAddon><MdPlace/></InputGroupAddon>
                    <Input onChange={this.setDepartureCity}
                           placeholder={this.state.data.departureCity}/>
                  </InputGroup>

                  <InputGroup>
                    <InputGroupAddon><MdAccessTime/></InputGroupAddon>
                    {/*<Input onChange={this.setDepartureTime}*/}
                           {/*placeholder={moment(this.state.data.departureTime).format('MMMM Do YYYY, h:mm:ss a')}/>*/}
                    <ListGroup>
                      <ListGroupItem>
                        <DatePicker
                          id="departure-date"
                          minDate={new Date(0)}
                          maxDate={new Date(2018, 12, 12)}
                          onChange={this.setDepartureTime}
                          returnValue="start"
                          value={this.state.data.departureTime ? new Date(this.state.data.departureTime) : new Date()}
                        />
                      </ListGroupItem>
                    </ListGroup>

                  </InputGroup>

                  <InputGroup>
                    <InputGroupAddon><MdPlace/></InputGroupAddon>
                    <Input onChange={this.setArrivalCity}
                           placeholder={this.state.data.arrivalCity}/>
                  </InputGroup>

                  <InputGroup>
                    <InputGroupAddon><MdAccessTime/></InputGroupAddon>
                    <ListGroup>
                      <ListGroupItem>
                        <DatePicker
                          id="arrival-date"
                          minDate={new Date(0)}
                          maxDate={new Date(2018, 12, 12)}
                          onChange={this.setArrivalTime}
                          returnValue="end"
                          value={this.state.data.arrivalTime ? new Date(this.state.data.arrivalTime) : new Date()}
                        />
                      </ListGroupItem>
                    </ListGroup>
                  </InputGroup>

                </ModalBody>
                <ModalFooter>
                  <Button color="success" onClick={this.update}>Update</Button>
                  <Button color="danger" onClick={this.delete}>Delete</Button>
                  <Button color="secondary" onClick={this.toggle}>Close</Button>
                </ModalFooter>
              </Modal> :
              null
          }
        </div>
    );
  }
}
