import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Button} from 'reactstrap';

export default class LandingPage extends Component {
  static propTypes = {};

  redirect = () => {
    this.props.history.push('/redirect');
  };

  render() {
    return (
      <div>
        <Button color="primary" onClick={this.redirect}>Go to flights</Button>
      </div>
    );
  }
}
