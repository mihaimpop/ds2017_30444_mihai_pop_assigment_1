import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {InputGroup, InputGroupAddon, Input} from 'reactstrap';
import {MdSearch} from 'react-icons/lib/md';

import CardsLayout from '../CardsLayout/CardsLayout.component';
import InfoModal from '../InfoModal/InfoModal.component';
import ROLES from '../../../services/roles';
import {getFlights} from '../../../services/resources/flight';

export default class UserPage extends Component {
  static propTypes = {
    loginInfo: PropTypes.object,
    getUserInfo: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.state = {
      flights: [],
      modal: false,
      searchString: null,
      unfilteredFlights: []
    };
  }

  componentWillMount() {
    this.props.getUserInfo(localStorage.getItem('username'))
      .then((userInfo) => {
        if (userInfo && userInfo.data.role !== ROLES.TYPE.USER) {
          this.props.history.push('/unauthorized');
        }
      });
  }

  componentDidMount() {
    this.getFlightsInfo();
  }

  getFlightsInfo = () => {

    getFlights().then(({data}) => {
      this.setState({flights: data, unfilteredFlights: data});
    });
  };

  openInfoModal = (data) => {
    this.modal.toggle(data);
  };

  filterList = (e) => {
    const searchString = e.target.value;

    let filteredFlights = this.state.flights
      .filter(({airplaneType, arrivalCity, departureCity, flightNumber}) => {
        return airplaneType.toLowerCase().includes(searchString) ||
          departureCity.toLowerCase().includes(searchString) ||
          arrivalCity.toLowerCase().includes(searchString) ||
          departureCity.toLowerCase().includes(searchString) ||
          flightNumber.toString().includes(searchString);
      });
    if (!filteredFlights.length) {
      filteredFlights = this.state.unfilteredFlights;
    }
    this.setState({
      flights: searchString === '' ? this.state.unfilteredFlights : filteredFlights
    });
  };

  render() {
    return (
      <div className="user-page">
        <div className="page-title">
          <span>@user</span>
          <div className="search-container">
            <InputGroup>
              <Input onChange={this.filterList} placeholder="Search..."/>
              <InputGroupAddon><MdSearch/></InputGroupAddon>
            </InputGroup>
          </div>
        </div>
        <InfoModal ref={(elem) => {
          this.modal = elem;
        }} openModal={this.openInfoModal}/>
        <CardsLayout data={this.state.flights} openModal={this.openInfoModal}/>
      </div>
    );
  }
}
