import React, {Component} from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import {
  Button, Modal, ModalHeader, ModalBody, ModalFooter,
  InputGroup, InputGroupAddon, Input
} from 'reactstrap';
import {MdAccessTime, MdAirplanemodeActive, MdPlace} from 'react-icons/lib/md';
import {FaTicket} from 'react-icons/lib/fa/';

import LocalTime from '../LocalTime/LocalTime.component';

export default class InfoModal extends Component {
  static propTypes = {
    data: PropTypes.object,
    openModal: PropTypes.func,
  };

  constructor(props) {
    super(props);
    this.state = {
      data: null || props.data,
      modal: false
    };
  }

  toggle = (data) => {
    this.setState({
      data: data,
      modal: !this.state.modal
    });

  };

  render() {
    return (
      <div className="info-modal-container">
        {
          this.state.data ?
            <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
              <ModalHeader toggle={this.toggle}>Flight #{this.state.data.flightNumber} Information</ModalHeader>
              <ModalBody>
                <InputGroup>
                  <InputGroupAddon><FaTicket/></InputGroupAddon>
                  <Input placeholder={`#${this.state.data.flightNumber}`}/>
                </InputGroup>

                <InputGroup>
                  <InputGroupAddon><MdAirplanemodeActive/></InputGroupAddon>
                  <Input placeholder={this.state.data.airplaneType}/>
                </InputGroup>

                <InputGroup>
                  <InputGroupAddon><MdPlace/></InputGroupAddon>
                  <Input placeholder={this.state.data.departureCity}/>
                </InputGroup>

                <InputGroup>
                  <InputGroupAddon><MdAccessTime/></InputGroupAddon>
                  <Input placeholder={moment(this.state.data.departureTime).format('MMMM Do YYYY, h:mm:ss a')}/>
                </InputGroup>

                <InputGroup>
                  <InputGroupAddon><MdPlace/></InputGroupAddon>
                  <Input placeholder={this.state.data.arrivalCity}/>
                </InputGroup>

                <InputGroup>
                  <InputGroupAddon><MdAccessTime/></InputGroupAddon>
                  <Input placeholder={moment(this.state.data.arrivalTime).format('MMMM Do YYYY, h:mm:ss a')}/>
                </InputGroup>

                <LocalTime departureCity={this.state.data.departureCity}
                           departureTime={this.state.data.departureTime}
                           arrivalCity={this.state.data.arrivalCity}
                           arrivalTime={this.state.data.arrivalTime}/>
              </ModalBody>
              <ModalFooter>
                <Button color="secondary" onClick={this.toggle}>Close</Button>
              </ModalFooter>
            </Modal> :
            null
        }
      </div>
    );
  }
}
