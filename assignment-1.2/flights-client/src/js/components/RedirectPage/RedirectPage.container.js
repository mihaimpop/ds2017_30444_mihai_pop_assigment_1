import {connect} from 'react-redux';

import RedirectPage from './RedirectPage.component';

const mapStateToProps = (state) => ({
  loginInfo: state.loginInfo.get('userInfo')
});

const mapDispatchToProps = (dispatch) => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(RedirectPage);
