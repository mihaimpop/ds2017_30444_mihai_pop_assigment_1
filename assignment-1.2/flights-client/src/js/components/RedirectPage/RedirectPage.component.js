import React, {Component} from 'react';
import PropTypes from 'prop-types';

import ROLES from '../../../services/roles';
import {getFlights} from '../../../services/resources/flight'

export default class RedirectPage extends Component {
  static propTypes = {
    loginInfo: PropTypes.object
  };

  constructor(props) {
    super(props);

  }

  componentDidMount() {
    //triggers auth form
    const {loginInfo} = this.props;
    if (!Object.keys(loginInfo).length) {
      return;
    }
    localStorage.setItem('username', loginInfo.data.userName);
    if (loginInfo && loginInfo.data.role === ROLES.TYPE.ADMIN) {
      this.props.history.push('/admin-flights');
    } else if (loginInfo && loginInfo.data.role === ROLES.TYPE.USER) {
      this.props.history.push('/user-flights');
    } else {
      this.props.history.push('/');
    }
  }

  render() {
    return (
      <div>
        Logging in...
      </div>
    );
  }
}
