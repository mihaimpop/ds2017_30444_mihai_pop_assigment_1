import httpResource from '../httpResource';

export const getFlight = (flightNumber) => {
  return httpResource.get(`api/flight/${flightNumber}`);
};

export const getFlights = (config) => {
  return httpResource.get(`api/flights`, config);
};

export const addFlight = (flight) => {
  return httpResource.post(`api/flight`, flight);
};

export const updateFlight = (flight) => {
  return httpResource.put(`api/flight`, flight);
};

export const deleteFlight = (flightNumber) => {
  return httpResource.delete(`api/flight/${flightNumber}`);
};