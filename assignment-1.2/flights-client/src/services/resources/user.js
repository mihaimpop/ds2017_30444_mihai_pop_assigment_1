import httpResource from '../httpResource';

export const getUser = (username) => {
  return httpResource.get(`api/user/${username}`);
};

export const addUser = (user) => {
  return httpResource.post(`api/user`, user);
};

export const updateUser = (user) => {
  return httpResource.put(`api/user`, user);
};

export const deleteUser = (username) => {
  return httpResource.delete(`api/user/${username}`);
};