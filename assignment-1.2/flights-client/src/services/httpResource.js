import axios from 'axios';
const CancelToken = axios.CancelToken;

/**
 * This is a cancelable resource. It decorates axios, the promise based HTTP client.
 */
export default class HttpResource{
  static REQUEST_TYPE = {
    REQUEST: 'REQUEST',
    GET: 'GET',
    DELETE: 'DELETE',
    HEAD: 'HEAD',
    POST: 'POST',
    PUT: 'PUT',
    PATCH: 'PATCH'
  };

  static REQUEST_CANCELED = 'REQUEST_CANCELED';

  static makeRequest = (action) => (url, data, config) => {
    const source = CancelToken.source(),
      cancelToken = source.token,
      requestConfig = config ? {config, cancelToken} : {cancelToken};

    let promise = null;

    switch(action){
      case HttpResource.REQUEST_TYPE.REQUEST: {
        promise = axios.request(requestConfig);
        break;
      }
      case HttpResource.REQUEST_TYPE.GET: {
        promise = axios.get(url, requestConfig);
        break;
      }
      case HttpResource.REQUEST_TYPE.DELETE: {
        promise = axios.delete(url, requestConfig);
        break;
      }
      case HttpResource.REQUEST_TYPE.HEAD: {
        promise = axios.head(url, requestConfig);
        break;
      }
      case HttpResource.REQUEST_TYPE.POST: {
        promise = axios.post(url, data, requestConfig);
        break;
      }
      case HttpResource.REQUEST_TYPE.PUT: {
        promise = axios.put(url, data, requestConfig);
        break;
      }
      case HttpResource.REQUEST_TYPE.PATCH: {
        promise = axios.patch(url, data, requestConfig);
        break;
      }
      default: {
        promise = axios.get(url, requestConfig);
        break;
      }
    }

    promise.cancel = source.cancel.bind(null, HttpResource.REQUEST_CANCELED);

    return promise;
  };

  static request = (config) => HttpResource.makeRequest(HttpResource.REQUEST_TYPE.REQUEST)(null, null, config);

  static get = (url, config) => HttpResource.makeRequest(HttpResource.REQUEST_TYPE.GET)(url, null, config);

  static delete = (url, config) => HttpResource.makeRequest(HttpResource.REQUEST_TYPE.DELETE)(url, null, config);

  static head = (url, config) => HttpResource.makeRequest(HttpResource.REQUEST_TYPE.HEAD)(url, null, config);

  static post = (url, data, config) => HttpResource.makeRequest(HttpResource.REQUEST_TYPE.POST)(url, data, config);

  static put = (url, data, config) => HttpResource.makeRequest(HttpResource.REQUEST_TYPE.PUT)(url, data, config);

  static patch = (url, data, config) => HttpResource.makeRequest(HttpResource.REQUEST_TYPE.PATCH)(url, data, config);
}
