export default class ROLES {
  static TYPE = {
    ADMIN: 'ROLE_ADMIN',
    USER: 'ROLE_USER'
  }
}