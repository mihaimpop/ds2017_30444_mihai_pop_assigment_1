const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

const distDirectory = path.resolve(__dirname, 'dist');

const extractSass = new ExtractTextPlugin({
  filename: '[name].css'
});

const copyHtml = new HtmlWebpackPlugin({
  filename: './index.html',
  hash: true, // Append a unique compilation hash to all included scripts and css files
  inject: 'body', // Inject all scripts into the body
  template: './index.html', // Load a custom template
  title: 'Flights' // Set page title
});

module.exports = {
  entry: ['./src/js/app.js'],
  output: {
    filename: 'bundle.js',
    path: distDirectory
  },
  devServer: {
    contentBase: distDirectory,
    historyApiFallback: true,
    hot: true,
  },
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['es2015', 'react', 'stage-2'],
            sourceMap: true
          }
        }
      },
      {
        test: /\.sass|scss$/,
        use: extractSass.extract({
          use: [{
            loader: 'css-loader',
            options: {
              sourceMap: true
            }
          }, {
            loader: 'sass-loader',
            options: {
              sourceMap: true
            }
          }]
        })
      },
      {
        test: /\.css$/,
        use: extractSass.extract({
          use: {
            loader: 'css-loader',
            options: {
              sourceMap: true
            }
          }
        })
      }
    ]
  },
  plugins: [
    extractSass,
    copyHtml
  ]
};
