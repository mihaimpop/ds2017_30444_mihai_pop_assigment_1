package com.assignment.flights.dao;

import com.assignment.flights.entity.Flight;
import java.util.List;

public interface IFlightDAO {
  List<Flight> getAllFlights();
  Flight getFlightByNumber(int flightNumber);
  void addFlight(Flight flight);
  void updateFlight(Flight newFlight);
  void deleteFlight(int flightNumber);
  boolean flightExists(int flightNumber);
}
