package com.assignment.flights.dao;

import com.assignment.flights.entity.Flight;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Transactional
@Repository
public class FlightDAO implements IFlightDAO{
  @PersistenceContext
  private EntityManager entityManager;

  @SuppressWarnings("unchecked")
  @Override
  public List<Flight> getAllFlights() {
    String hql = "FROM Flight";
    return (List<Flight>) entityManager.createQuery(hql).getResultList();
  }

  @Override
  public Flight getFlightByNumber(int flightNumber) {
    return entityManager.find(Flight.class, flightNumber);
  }

  @Override
  public void addFlight(Flight flight) {
    entityManager.persist(flight);
  }

  @Override
  public void updateFlight(Flight newFlight) {
    Flight flight = getFlightByNumber(newFlight.getFlightNumber());
    flight.setAirplaneType(newFlight.getAirplaneType());
    flight.setDepartureCity(newFlight.getDepartureCity());
    flight.setDepartureTime(newFlight.getDepartureTime());
    flight.setArrivalCity(newFlight.getArrivalCity());
    flight.setArrivalTime(newFlight.getArrivalTime());
    entityManager.flush();
  }

  @Override
  public void deleteFlight(int flightNumber) {
    entityManager.remove(getFlightByNumber(flightNumber));
  }

  @Override
  public boolean flightExists(int flightNumber) {
    String hql = "FROM Flight as flght WHERE flght.flightNumber = ?";
    int count = entityManager.createQuery(hql).setParameter(1, flightNumber)
      .getResultList().size();
    return count > 0 ? true : false;
  }

}
