package com.assignment.flights.dao;

import com.assignment.flights.entity.UserInfo;

public interface IUserInfoDAO {
  UserInfo getActiveUser(String userName);
  UserInfo getUserByName(String userName);
  void addUser(UserInfo user);
  void updateUser(UserInfo newUser);
  void deleteUser(String userName);
  boolean userExists(String userName);
}
