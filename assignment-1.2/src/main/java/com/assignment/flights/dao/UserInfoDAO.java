package com.assignment.flights.dao;

import com.assignment.flights.entity.UserInfo;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Transactional
public class UserInfoDAO implements IUserInfoDAO {
  @PersistenceContext
  private EntityManager entityManager;

  public UserInfo getActiveUser(String userName) {
    UserInfo activeUserInfo = new UserInfo();
    short enabled = 1;
    List<?> list = entityManager.createQuery("SELECT u FROM UserInfo u WHERE userName=? and enabled=?")
      .setParameter(1, userName).setParameter(2, enabled).getResultList();
    if (!list.isEmpty()) {
      activeUserInfo = (UserInfo) list.get(0);
    }
    return activeUserInfo;
  }

  @Override
  public UserInfo getUserByName(String userName) {
    return entityManager.find(UserInfo.class, userName);
  }

  @Override
  public void addUser(UserInfo user) {
    entityManager.persist(user);
  }

  @Override
  public void updateUser(UserInfo newUser) {
    UserInfo user = getUserByName(newUser.getUserName());
    short enabled = 1;

    user.setPassword(newUser.getPassword());
    user.setFullName(newUser.getFullName());
    user.setRole(newUser.getRole());
    user.setEnabled(enabled);
    entityManager.flush();
  }

  @Override
  public void deleteUser(String username) {
    entityManager.remove(getUserByName(username));
  }

  @Override
  public boolean userExists(String username) {
    String hql = "FROM UserInfo as ui WHERE ui.userName = ?";
    int count = entityManager.createQuery(hql).setParameter(1, username)
      .getResultList().size();
    return count > 0 ? true : false;
  }
}
