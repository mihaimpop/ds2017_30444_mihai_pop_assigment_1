package com.assignment.flights.controller;

import com.assignment.flights.entity.Flight;
import com.assignment.flights.service.IFlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@CrossOrigin(maxAge = 3600)
@Controller
@RequestMapping("api")
public class FlightController {
  @Autowired
  private IFlightService flightService;

  @GetMapping("flight/{flightNumber}")
  public ResponseEntity<Flight> getFlightByNumber(@PathVariable("flightNumber") int flightNumber) {
    Flight flight = flightService.getFlightByNumber(flightNumber);
    return new ResponseEntity<Flight>(flight, HttpStatus.OK);
  }

  @GetMapping("flights")
  public ResponseEntity<List<Flight>> getAllFlights() {
    List<Flight> list = flightService.getAllFlights();
    return new ResponseEntity<List<Flight>>(list, HttpStatus.OK);
  }

  @PostMapping("flight")
  public ResponseEntity<Void> addFlight(@RequestBody Flight flight, UriComponentsBuilder builder) {
    boolean canAddFlight = flightService.addFlight(flight);
    if (!canAddFlight) {
      return new ResponseEntity<Void>(HttpStatus.CONFLICT);
    }
    HttpHeaders headers = new HttpHeaders();
    headers.setLocation(builder.path("/flight/{flightNumber}").buildAndExpand(flight.getFlightNumber()).toUri());
    return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
  }

  @PutMapping("flight")
  public ResponseEntity<Flight> updateFlight(@RequestBody Flight flight) {
    flightService.updateFlight(flight);
    return new ResponseEntity<Flight>(flight, HttpStatus.OK);
  }

  @DeleteMapping("flight/{flightNumber}")
  public ResponseEntity<Void> deleteFlight(@PathVariable("flightNumber") int flightNumber) {
    flightService.deleteFlight(flightNumber);
    return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
  }
}
