package com.assignment.flights.controller;

import com.assignment.flights.entity.UserInfo;
import com.assignment.flights.service.IUserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

@CrossOrigin(maxAge = 3600)
@Controller
@RequestMapping("api")
public class UserInfoController {
  @Autowired
  private IUserInfoService userInfoService;

  @GetMapping("user/{userName}")
  public ResponseEntity<UserInfo> getUserByName(@PathVariable("userName") String userName) {
    UserInfo user = userInfoService.getUserByName(userName);
    return new ResponseEntity<UserInfo>(user, HttpStatus.OK);
  }

  @PostMapping("user")
  public ResponseEntity<Void> addUser(@RequestBody UserInfo user, UriComponentsBuilder builder) {
    boolean canAddUser = userInfoService.addUser(user);
    if (!canAddUser) {
      return new ResponseEntity<Void>(HttpStatus.CONFLICT);
    }
    HttpHeaders headers = new HttpHeaders();
    headers.setLocation(builder.path("/user/{userName}").buildAndExpand(user.getUserName()).toUri());
    return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
  }

  @PutMapping("user")
  public ResponseEntity<UserInfo> updateUser(@RequestBody UserInfo user) {
    userInfoService.updateUser(user);
    return new ResponseEntity<UserInfo>(user, HttpStatus.OK);
  }

  @DeleteMapping("user/{userName}")
  public ResponseEntity<Void> deleteUser(@PathVariable("userName") String userName) {
    userInfoService.deleteUser(userName);
    return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
  }
}
