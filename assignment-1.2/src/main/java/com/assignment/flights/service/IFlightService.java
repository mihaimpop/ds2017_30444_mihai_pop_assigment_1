package com.assignment.flights.service;

import com.assignment.flights.entity.Flight;
import org.springframework.security.access.annotation.Secured;

import java.util.List;

public interface IFlightService {
  @Secured({"ROLE_ADMIN", "ROLE_USER"})
  List<Flight> getAllFlights();

  @Secured({"ROLE_ADMIN"})
  Flight getFlightByNumber(int flightNumber);

  @Secured({"ROLE_ADMIN"})
  boolean addFlight(Flight flight);

  @Secured({"ROLE_ADMIN"})
  void updateFlight(Flight flight);

  @Secured({"ROLE_ADMIN"})
  void deleteFlight(int flightNumber);
}
