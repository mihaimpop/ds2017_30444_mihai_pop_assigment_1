package com.assignment.flights.service;

import com.assignment.flights.dao.IFlightDAO;
import com.assignment.flights.entity.Flight;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FlightService implements IFlightService {
  @Autowired
  private IFlightDAO flightDAO;

  @Override
  public List<Flight> getAllFlights() {
    return flightDAO.getAllFlights();
  }

  @Override
  public Flight getFlightByNumber(int flightNumber) {
    return flightDAO.getFlightByNumber(flightNumber);
  }

  @Override
  public synchronized boolean addFlight(Flight flight) {
    if (flightDAO.flightExists(flight.getFlightNumber())) {
      return false;
    } else {
      flightDAO.addFlight(flight);
      return true;
    }
  }

  @Override
  public void updateFlight(Flight newFlight) {
    flightDAO.updateFlight(newFlight);
  }

  @Override
  public void deleteFlight(int flightNumber) {
    flightDAO.deleteFlight(flightNumber);
  }
}
