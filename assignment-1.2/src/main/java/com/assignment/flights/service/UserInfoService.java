package com.assignment.flights.service;

import com.assignment.flights.dao.IUserInfoDAO;
import com.assignment.flights.entity.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserInfoService implements IUserInfoService{
  @Autowired
  private IUserInfoDAO userInfoDAO;

  @Override
  public UserInfo getUserByName(String userName) {
    return userInfoDAO.getUserByName(userName);
  }

  @Override
  public synchronized boolean addUser(UserInfo user) {
    if(userInfoDAO.userExists(user.getUserName())) {
      return false;
    } else {
      userInfoDAO.addUser(user);
      return true;
    }
  }

  @Override
  public void updateUser(UserInfo newUser) {
    userInfoDAO.updateUser(newUser);
  }

  @Override
  public void deleteUser(String userName) {
    userInfoDAO.deleteUser(userName);
  }
}
