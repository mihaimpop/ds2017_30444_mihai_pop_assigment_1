package com.assignment.flights.service;

import com.assignment.flights.entity.UserInfo;
import org.springframework.security.access.annotation.Secured;

public interface IUserInfoService {
  @Secured({"ROLE_ADMIN", "ROLE_USER"})
  UserInfo getUserByName(String userName);

  @Secured({"ROLE_ADMIN", "ROLE_USER"})
  boolean addUser(UserInfo user);

  @Secured({"ROLE_ADMIN", "ROLE_USER"})
  void updateUser(UserInfo newUser);

  @Secured({"ROLE_ADMIN", "ROLE_USER"})
  void deleteUser(String userName);
}
