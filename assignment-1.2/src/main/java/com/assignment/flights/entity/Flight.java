package com.assignment.flights.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "flights")
public class Flight {
  private static final long serialVersionUID = 1L;
  @Id
  @Column(name = "flight_number")
  private int flightNumber;

  @Column(name = "airplane_type")
  private String airplaneType;

  @Column(name = "departure_city")
  private String departureCity;

  @Column(name = "departure_time")
  private Timestamp departureTime;

  @Column(name = "arrival_city")
  private String arrivalCity;

  @Column(name = "arrival_time")
  private Timestamp arrivalTime;

  public static long getSerialVersionUID() {
    return serialVersionUID;
  }

  public int getFlightNumber() {
    return flightNumber;
  }

  public void setFlightNumber(int flightNumber) {
    this.flightNumber = flightNumber;
  }

  public String getAirplaneType() {
    return airplaneType;
  }

  public void setAirplaneType(String airplaneType) {
    this.airplaneType = airplaneType;
  }

  public String getDepartureCity() {
    return departureCity;
  }

  public void setDepartureCity(String departureCity) {
    this.departureCity = departureCity;
  }

  public Timestamp getDepartureTime() {
    return departureTime;
  }

  public void setDepartureTime(Timestamp departureTime) {
    this.departureTime = departureTime;
  }

  public String getArrivalCity() {
    return arrivalCity;
  }

  public void setArrivalCity(String arrivalCity) {
    this.arrivalCity = arrivalCity;
  }

  public Timestamp getArrivalTime() {
    return arrivalTime;
  }

  public void setArrivalTime(Timestamp arrivalTime) {
    this.arrivalTime = arrivalTime;
  }
}
