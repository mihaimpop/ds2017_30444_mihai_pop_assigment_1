package com.assignment.flights;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

public class RestClientUtil {
  private static HttpHeaders getHeaders(String credential) {
//    String credential="mukesh:m123";
    //String credential="tarun:t123";
    String encodedCredential = new String(Base64.encodeBase64(credential.getBytes()));
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    headers.add("Authorization", "Basic " + encodedCredential);
    return headers;
  }

  public static void main(String args[]) {
    RestClientUtil util = new RestClientUtil();
    HttpHeaders headers = getHeaders("andrei:andrei123");
    System.out.println(headers.toString());
  }
}
