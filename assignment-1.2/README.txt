- open the pom.xml via Intellij IDEA or you preferred IDE
- Run the main class of the application

- install Node.js on your machine
- open a terminal/ command prompt/ git bash inside flights-client folder
- type 'npm install'
- hit enter to install the client aplication's dependencies

- inside flights-client folder open a terminal window
- type 'npm run watch'. this is a macro for the command 'webpack --watch -d --progress --colors'
- hit enter to bundle our frontend code

- in another terminal window in the same folder type 'npm run server'. this starts a local server which proxies frontend requests through port 8080.
- hit enter and wait for it to start

- open a browser window on:
	http://localhost:3000 
